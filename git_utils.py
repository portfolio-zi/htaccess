import sys
import os.path
import subprocess as sp

def exec_command(command_list):
    # print command_list
    p = sp.Popen(command_list, stdout=sp.PIPE,
                 stderr=sp.PIPE)
    out, err = p.communicate()
    return out, err

def get_basedir():
    out, err = exec_command(['git', 'rev-parse', '--show-toplevel']) 
    return out.strip()

def get_project_name():
    basedir = get_basedir()
    return os.path.basename(basedir).strip()

def get_submodules():
    out =  exec_command(["grep", "path", ".gitmodules"])
    modules = out[0].strip().split('\n')
    #print modules
    for i in range(len(modules)):
        modules[i] = modules[i].split('=')[1].strip()
    return modules

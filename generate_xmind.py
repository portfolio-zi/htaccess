#!/usr/bin/python

PROJECT_NAME = "NIBS2012"

from mekk.xmind import XMindDocument

topics_file = open('topics.txt','r')

subtopics = topics_file.readlines()
print subtopics

OUTPUT = PROJECT_NAME+".xmind"
xmind = XMindDocument.create(u"Project Planing", PROJECT_NAME)
first_sheet = xmind.get_first_sheet()
root_topic = first_sheet.get_root_topic()

for subtopic in subtopics:
    subtopic = subtopic.strip()
    root_topic.add_subtopic(subtopic)


xmind.save(OUTPUT)

#xmind.pretty_print()

print "Saved to", OUTPUT
